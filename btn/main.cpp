#include "clhm.h" // Inclut le fichier d'en-tête de la classe clhm

#include <QApplication> // Inclut la classe QApplication

int main(int argc, char *argv[])
{
    QApplication a(argc, argv); // Crée une instance de QApplication avec les arguments fournis

    clhm w; // Crée une instance de la classe clhm,
    w.show(); // Affiche l'instance de la classe clhm
    return a.exec(); // Exécute la boucle principale de l'application, attend que l'application se termine
}
